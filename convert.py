from datetime import date, datetime
from xml.dom import minidom
from time import strptime, strftime

xmldoc = minidom.parse('temp.xml')
itemlist = xmldoc.getElementsByTagName('tempiesiri')
produse = {}

for s in itemlist:
  cod = None
  cantitate = None
  denumire = None
  data = None
  pretVanzare = None
  um = None
  gestiune = None

  for child in s.childNodes:
    if 'cod' == child.nodeName:
      cod = child.firstChild.nodeValue
    elif 'cantitate' == child.nodeName:
      cantitate = child.firstChild.nodeValue
    elif 'denumire' == child.nodeName:
      denumire = child.firstChild.nodeValue
    elif 'data' == child.nodeName:
      data = child.firstChild.nodeValue
    elif 'pret_van' == child.nodeName:
      pretVanzare = child.firstChild.nodeValue
    elif 'um' == child.nodeName:
      um = child.firstChild.nodeValue
    elif 'cod_gest' == child.nodeName:
      gestiune = child.firstChild.nodeValue

  produs = {
    'cod': int(cod),
    'denumire': denumire,
    'cantitate': float(cantitate),
    'pret': float(pretVanzare),
    'data': data,
    'um': um,
    'gestiune': gestiune,
  }
  
  try:
    lista = produse[data]
  except KeyError:
    lista = []
    produse[data] = lista
  
  lista.append(produs)
  
  nr_monetare = len(produse.keys())
  
  f = open('monetar.ini', 'w')
  
  # Header
  now = date.today()
  f.write('[InfoPachet]\n')
  f.write('AnLucru=%d\n' % now.year)
  f.write('LunaLucru=%d\n' % now.month)
  f.write('TipDocument=MONETAR\n')
  f.write('TotalMonetare=%d\n' % nr_monetare)
  f.write('\n')
    
  for idx in range(0, nr_monetare):
    data = produse.keys()[idx]
    elemente = produse[data]
    parsed_data = strptime(data, '%Y-%m-%dT%H:%M:%S')
  
    f.write('[Monetar_%d]\n' % (idx + 1))
    f.write('Operat=D\n')
    f.write('NrDoc=%d\n' % (idx + 1))
    f.write('SimbolCarnet=M\n')
    f.write('Operatie=A\n')
    f.write('Data=%s\n' % strftime('%d.%m.%Y', parsed_data))
    f.write('Casa=CasaLei\n')
    f.write('TotalArticole=%d\n' % len(elemente))
    f.write('CEC=0\n')
    f.write('CARD=0\n')
    f.write('BONVALORIC=0\n')
    f.write('Observatii=\n')
    f.write('Discount=0.0\n')
    f.write('TVADiscount=0.0\n')
    f.write('\n')    
    
    f.write('[Items_%d]\n' % (idx + 1))
    for itm in range(0, len(elemente)):
      item = elemente[itm]
      
      f.write('Item_%d=%d;%s;%d;%.2f;%s;\n' % (itm + 1, item['cod'], item['um'], item['cantitate'], item['pret'], item['gestiune']))
      
    f.write('\n')        
  
  f.close()